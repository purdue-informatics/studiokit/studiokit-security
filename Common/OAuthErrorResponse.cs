﻿namespace StudioKit.Security.Common;

/// <summary>
/// https://datatracker.ietf.org/doc/html/rfc6749#section-5.2
/// </summary>
public static class OAuthErrorResponse
{
	public const string InvalidRequest = "invalid_request";

	public const string InvalidClient = "invalid_client";

	public const string InvalidGrant = "invalid_grant";

	public const string UnauthorizedClient = "unauthorized_client";

	public const string UnsupportedGrantType = "unsupported_grant_type";

	public const string InvalidScope = "invalid_scope";
}