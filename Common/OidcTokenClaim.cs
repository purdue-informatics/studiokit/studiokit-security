﻿namespace StudioKit.Security.Common;

/// <summary>
/// https://www.iana.org/assignments/jwt/jwt.xhtml
/// </summary>
public static class OidcTokenClaim
{
	/// <summary>
	/// Identifier for the party to which this token was issued
	/// </summary>
	public const string AuthorizedParty = "azp";

	/// <summary>
	/// Value used to associate a Client session with an ID Token
	/// </summary>
	public const string Nonce = "nonce";

	public const string Email = "email";

	public const string GivenName = "given_name";

	public const string FamilyName = "family_name";

	public const string Name = "name";

	public const string MiddleName = "middle_name";

	/// <summary>
	/// Profile picture URL
	/// </summary>
	public const string Picture = "picture";

	public const string Locale = "locale";
}