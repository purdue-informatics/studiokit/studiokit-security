﻿namespace StudioKit.Security.Common;

/// <summary>
/// https://www.iana.org/assignments/jwt/jwt.xhtml
/// </summary>
public static class JsonWebTokenClaim
{
	/// <summary>
	/// Identifier for the object this token refers to
	/// </summary>
	public const string Subject = "sub";

	/// <summary>
	/// Identifier for the creator of the token
	/// </summary>
	public const string Issuer = "iss";

	/// <summary>
	/// Identifier for a party intended to receive this token, may have more than one
	/// </summary>
	public const string Audience = "aud";

	/// <summary>
	/// Time the token was issued as seconds since the Unix epoch
	/// </summary>
	public const string IssuedAt = "iat";

	/// <summary>
	/// Time the token will expire as seconds since the Unix epoch
	/// </summary>
	public const string Expiration = "exp";

	/// <summary>
	/// Unique ID for this token
	/// </summary>
	public const string JwtId = "jti";
}