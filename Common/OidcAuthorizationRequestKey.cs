﻿namespace StudioKit.Security.Common;

/// <summary>
/// https://openid.net/specs/openid-connect-core-1_0.html
/// </summary>
public static class OidcAuthorizationRequestKey
{
	public static readonly string ResponseType = OAuthRequestKey.ResponseType;

	public static readonly string ClientId = OAuthRequestKey.ClientId;

	public static readonly string Scope = OAuthRequestKey.Scope;

	public static readonly string State = OAuthRequestKey.State;

	public const string RedirectUri = OAuthRequestKey.RedirectUri;

	public static readonly string Nonce = OidcTokenClaim.Nonce;

	public const string ResponseMode = "response_mode";

	public const string Display = "display";

	public const string Prompt = "prompt";

	public const string LoginHint = "login_hint";

	public const string ClientAssertion = "client_assertion";

	public const string ClientAssertionType = "client_assertion_type";

	public const string JwtBearerAssertionType = "urn:ietf:params:oauth:client-assertion-type:jwt-bearer";

	#region Lti

	public const string LtiMessageHint = "lti_message_hint";

	public const string LtiDeploymentId = "lti_deployment_id";

	#endregion Lti
}