﻿namespace StudioKit.Security.Common;

/// <summary>
/// https://tools.ietf.org/html/rfc6749
/// </summary>
public static class OAuthRequestKey
{
	public const string ResponseType = "response_type";

	public const string ClientId = "client_id";

	public const string ClientSecret = "client_secret";

	public const string Scope = "scope";

	public const string GrantType = "grant_type";

	public const string State = "state";

	public const string RedirectUri = "redirect_uri";

	public const string Code = "code";

	public const string RefreshToken = "refresh_token";
}