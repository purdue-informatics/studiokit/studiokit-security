﻿namespace StudioKit.Security.Common;

public static class OidcResponseType
{
	public const string Code = "code";

	public const string IdToken = "id_token";

	public const string Token = "token";
}