﻿namespace StudioKit.Security.Common;

public static class OAuthGrantType
{
	public const string Password = "password";

	public const string AuthorizationCode = "authorization_code";

	public const string RefreshToken = "refresh_token";

	public const string ClientCredentials = "client_credentials";
}