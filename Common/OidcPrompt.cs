﻿namespace StudioKit.Security.Common;

public static class OidcPrompt
{
	public const string None = "none";

	public const string Login = "login";

	public const string Consent = "consent";

	public const string SelectAccount = "select_account";
}