﻿# StudioKit.Security

**Note**: Requires the following peer dependencies in the Solution.
* **StudioKit.Data**

## Models

* Client
	* Base class for other types of clients.
* AppClient
	* A client for use by an app, e.g. Web, iOS, Android. Uses `AuthorizationGrantType.AuthorizationCode` and `AuthorizationGrantType.RefreshToken`.
* ServiceClient
	* A client for use by an external service. Uses `AuthorizationGrantType.ClientCredentials`.
* RefreshToken
	* Model for storing a refresh token.
