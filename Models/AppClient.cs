using StudioKit.Security.Common;

namespace StudioKit.Security.Models;

/// <summary>
/// A client for use by an app, e.g. Web, iOS, Android.
/// </summary>
public class AppClient : Client
{
	public AppClient()
	{
		Grants = string.Join(",", OAuthGrantType.AuthorizationCode, OAuthGrantType.RefreshToken);
	}

	public string CurrentVersion { get; set; }

	public string MinVersion { get; set; }
}