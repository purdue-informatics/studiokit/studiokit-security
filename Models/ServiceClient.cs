using StudioKit.Security.Common;
using System.ComponentModel.DataAnnotations;

namespace StudioKit.Security.Models;

/// <summary>
/// A client for use by an external service.
/// </summary>
public class ServiceClient : Client
{
	public ServiceClient()
	{
		Grants = OAuthGrantType.ClientCredentials;
	}

	[Required]
	public string Name { get; set; }
}