﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Security;
using StudioKit.Encryption;
using StudioKit.Security.BusinessLogic.Interfaces;

namespace StudioKit.Security.BusinessLogic.Services;

public class CertificateService : ICertificateService
{
	private readonly ICertificateConfigurationProvider _certificateConfigurationProvider;

	public CertificateService(ICertificateConfigurationProvider certificateConfigurationProvider)
	{
		_certificateConfigurationProvider = certificateConfigurationProvider ??
											throw new ArgumentNullException(nameof(certificateConfigurationProvider));
	}

	public async Task<SigningCredentials> GetSigningCredentialsAsync(CancellationToken cancellationToken = default)
	{
		var configuration = await _certificateConfigurationProvider.GetCertificateConfigurationAsync(cancellationToken);
		var keyId = configuration.DateLastUpdated.Ticks.ToString();
		var rsaParams = GetRsaParameters(configuration);
		var key = new RsaSecurityKey(rsaParams) { KeyId = keyId };
		return new SigningCredentials(key, SecurityAlgorithms.RsaSha256);
	}

	public async Task<JsonWebKeySet> GetJsonWebKeySetAsync(CancellationToken cancellationToken = default)
	{
		var configuration = await _certificateConfigurationProvider.GetCertificateConfigurationAsync(cancellationToken);
		var keyId = configuration.DateLastUpdated.Ticks.ToString();
		var keySet = new JsonWebKeySet();

		if (string.IsNullOrWhiteSpace(configuration.RsaKeyPair))
			return keySet;

		var rsaParams = GetRsaParameters(configuration);
		var key = new JsonWebKey
		{
			Alg = SecurityAlgorithms.RsaSha256,
			Kty = JsonWebAlgorithmsKeyTypes.RSA,
			Use = JsonWebKeyUseNames.Sig,
			Kid = keyId,
			E = Base64UrlEncoder.Encode(rsaParams.Exponent),
			N = Base64UrlEncoder.Encode(rsaParams.Modulus)
		};
		keySet.Keys.Add(key);
		return keySet;
	}

	private static RSAParameters GetRsaParameters(ICertificateConfiguration configuration)
	{
		var decryptedKeyPairString = EncryptedConfigurationManager.TryDecryptSettingValue(configuration.RsaKeyPair);

		using (var privateKeyReader = new StringReader(decryptedKeyPairString))
		{
			var keyPair = (AsymmetricCipherKeyPair)new PemReader(privateKeyReader).ReadObject();
			var privateKeyParams = (RsaPrivateCrtKeyParameters)keyPair.Private;
			var rsaParams = DotNetUtilities.ToRSAParameters(privateKeyParams);
			return rsaParams;
		}
	}
}