﻿using StudioKit.Security.BusinessLogic.Interfaces;
using StudioKit.Security.BusinessLogic.Models;
using System.Security.Principal;

namespace StudioKit.Security.BusinessLogic.Services;

/// <summary>
/// An <see cref="IPrincipalProvider"/> that returns a system principal <see cref="SystemPrincipal"/>.
/// To be used when a user specific principal is not available, and actions are being completed by the system itself.
/// </summary>
public class SystemPrincipalProvider : IPrincipalProvider
{
	public IPrincipal GetPrincipal()
	{
		return new SystemPrincipal();
	}
}