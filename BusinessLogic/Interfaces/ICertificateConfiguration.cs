﻿using System;

namespace StudioKit.Security.BusinessLogic.Interfaces;

public interface ICertificateConfiguration
{
	/// <summary>
	/// An encrypted RSA private/public key pair
	/// </summary>
	string RsaKeyPair { get; set; }

	DateTime DateLastUpdated { get; set; }
}