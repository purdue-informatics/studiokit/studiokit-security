﻿using System.Security.Principal;

namespace StudioKit.Security.BusinessLogic.Interfaces;

public interface IPrincipalProvider
{
	IPrincipal GetPrincipal();
}