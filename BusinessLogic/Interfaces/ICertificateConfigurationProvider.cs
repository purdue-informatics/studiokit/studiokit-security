﻿using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Security.BusinessLogic.Interfaces;

public interface ICertificateConfigurationProvider
{
	Task<ICertificateConfiguration> GetCertificateConfigurationAsync(CancellationToken cancellationToken = default);
}