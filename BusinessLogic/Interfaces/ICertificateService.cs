﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;

namespace StudioKit.Security.BusinessLogic.Interfaces;

public interface ICertificateService
{
	Task<SigningCredentials> GetSigningCredentialsAsync(CancellationToken cancellationToken = default);

	Task<JsonWebKeySet> GetJsonWebKeySetAsync(CancellationToken cancellationToken = default);
}