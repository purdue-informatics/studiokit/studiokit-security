﻿using NuGet.Versioning;
using StudioKit.Security.Models;
using System;

namespace StudioKit.Security.BusinessLogic.Extensions;

public static class AppClientExtensions
{
	public static SemanticVersion SemanticCurrentVersion(this AppClient appClient)
	{
		return appClient.CurrentVersion == null ? null : SemanticVersion.Parse(appClient.CurrentVersion);
	}

	public static SemanticVersion SemanticMinVersion(this AppClient appClient)
	{
		return appClient.MinVersion == null ? null : SemanticVersion.Parse(appClient.MinVersion);
	}

	public static bool IsVersionCurrent(this AppClient appClient, string version)
	{
		if (appClient.CurrentVersion == null)
			return true;
		if (version == null)
			throw new ArgumentNullException(nameof(version));

		var didParse = SemanticVersion.TryParse(version, out var semanticVersion);
		if (!didParse || semanticVersion == null)
			throw new Exception("version failed to Parse as SemanticVersion");

		return semanticVersion.CompareTo(appClient.SemanticCurrentVersion()) >= 0;
	}

	public static bool IsVersionSupported(this AppClient appClient, string version)
	{
		if (appClient.MinVersion == null)
			return true;
		if (version == null)
			throw new ArgumentNullException(nameof(version));

		var didParse = SemanticVersion.TryParse(version, out var semanticVersion);
		if (!didParse || semanticVersion == null)
			throw new Exception("version failed to Parse as SemanticVersion");

		return semanticVersion.CompareTo(appClient.SemanticMinVersion()) != -1;
	}
}