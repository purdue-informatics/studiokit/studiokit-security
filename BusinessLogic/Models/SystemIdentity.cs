﻿using System.Security.Principal;

namespace StudioKit.Security.BusinessLogic.Models;

/// <summary>
/// An <see cref="IIdentity"/> representing the system itself.
/// </summary>
public class SystemIdentity : IIdentity
{
	public string Name => "System";

	public string AuthenticationType => "Automatic";

	public bool IsAuthenticated => true;
}